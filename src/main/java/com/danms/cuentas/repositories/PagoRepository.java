package com.danms.cuentas.repositories;

import com.danms.cuentas.model.Pago;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PagoRepository extends JpaRepository<Pago, Integer>{

}
